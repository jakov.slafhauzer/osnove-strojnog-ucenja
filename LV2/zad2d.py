import numpy as np

data = np.genfromtxt('data (1).csv', delimiter=',')

visina = data[1:,1]

minimalna_visina = np.min(visina)
maksimalna_visina = np.max(visina)
srednja_visina = np.mean(visina)

print("Minimalna visina: ", minimalna_visina, "cm")
print("Maksimalna visina: ", maksimalna_visina, "cm")
print("Srednja visina: ", srednja_visina, "cm")