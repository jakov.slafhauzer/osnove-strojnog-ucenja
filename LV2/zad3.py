import numpy as np
import matplotlib.pyplot as plt

# Učitavanje slike
slika = plt.imread('road (1).jpg')
slika_array = np.array(slika)


plt.imshow(slika, alpha=0.5)
plt.title('Posvijetljena slika')
plt.axis('off')
plt.show()

# b) Prikaz samo druge četvrtine slike po širini
width = slika.shape
plt.figure(figsize=(8, 6))
plt.imshow(slika[:, int(width*(1/4)): int(width*(1/2))], cmap="gray")
plt.title('Druga četvrtina slike po širini')
plt.axis('off')
plt.show()

# c) Rotacija slike za 90 stupnjeva u smjeru kazaljke na satu
rotirana_slika = np.rot90(slika_array)

# Prikaz rotirane slike
plt.figure(figsize=(8, 6))
plt.imshow(rotirana_slika)
plt.title('Rotirana slika za 90 stupnjeva u smjeru kazaljke na satu')
plt.axis('off')
plt.show()

# d) Zrcaljenje slike
zrcaljena_slika = np.fliplr(slika_array)

# Prikaz zrcaljene slike
plt.figure(figsize=(8, 6))
plt.imshow(zrcaljena_slika)
plt.title('Zrcaljena slika')
plt.axis('off')
plt.show()
