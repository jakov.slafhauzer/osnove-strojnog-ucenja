import numpy as np

data = np.genfromtxt('data (1).csv', delimiter=',')

muskarci = (data[:, 0] == 1)
zene = (data[:, 0] == 0)

# Razdvajanje podataka u visinu za muškarce i žene
visina_muskarci = data[muskarci, 1]
visina_zene = data[zene, 1]

# Izračunavanje minimalne, maksimalne i srednje vrijednosti visine za muškarce
minimalna_visina_muskarci = np.min(visina_muskarci)
maksimalna_visina_muskarci = np.max(visina_muskarci)
srednja_visina_muskarci = np.mean(visina_muskarci)

# Izračunavanje minimalne, maksimalne i srednje vrijednosti visine za žene
minimalna_visina_zene = np.min(visina_zene)
maksimalna_visina_zene = np.max(visina_zene)
srednja_visina_zene = np.mean(visina_zene)

# Ispis rezultata za muškarce
print("Muškarci:")
print("Minimalna visina:", minimalna_visina_muskarci, "cm")
print("Maksimalna visina:", maksimalna_visina_muskarci, "cm")
print("Srednja visina:", srednja_visina_muskarci, "cm")

# Ispis rezultata za žene
print("\nŽene:")
print("Minimalna visina:", minimalna_visina_zene, "cm")
print("Maksimalna visina:", maksimalna_visina_zene, "cm")
print("Srednja visina:", srednja_visina_zene, "cm")