import numpy as np
import matplotlib.pyplot as plt 

data = np.genfromtxt('data (1).csv', delimiter=',')

visina = data[:, 1]
masa = data[:, 2]

visina_every_50 = visina[1::50]
masa_every_50 = masa[1::50]

plt.scatter(visina_every_50, masa_every_50, c=data[1::50, 0], cmap = "PiYG", alpha=0.5)
plt.title('Odnos visine i mase osoba')
plt.xlabel('Visina (cm)')
plt.ylabel('Masa (kg)')
plt.grid(True)
plt.show()