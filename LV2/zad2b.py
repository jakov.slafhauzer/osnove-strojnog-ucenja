import numpy as np
import matplotlib.pyplot as plt 

data = np.genfromtxt('data (1).csv', delimiter=',')

visina = data[:, 1]
masa = data[:, 2]

plt.scatter(visina, masa, c=data[:, 0], cmap = "PiYG", alpha=0.5)
plt.title('Odnos visine i mase osoba')
plt.xlabel('Visina (cm)')
plt.ylabel('Masa (kg)')
plt.grid(True)
plt.show()