#Napišite Python skriptu koja ce ucitati tekstualnu datoteku naziva song.txt.
#Potrebno je napraviti rjecnik koji kao kljuceve koristi sve razlicite rijeci koje se pojavljuju u 
#datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijec (kljuc) pojavljuje u datoteci. 
#Koliko je rijeci koje se pojavljuju samo jednom u datoteci? Ispišite ih.

data = open("LV1\song.txt")
dictionary = {}
for line in data:
    line = line.strip()
    line = line.lower()
    words = line.split()
    for word in words:
        if word in dictionary:
            dictionary[word] += 1
        else:
            dictionary[word] = 1

print(dictionary)
data.close()
