#Napišite program koji od korisnika zahtijeva unos brojeva u beskonacnoj petlji
#sve dok korisnik ne upiše „Done“ (bez navodnika). Pri tome brojeve spremajte u listu. Nakon toga
#potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
#vrijednost. Sortirajte listu i ispišite je na ekran. Dodatno: osigurajte program od pogrešnog unosa
#(npr. slovo umjesto brojke) na nacin da program zanemari taj unos i ispiše odgovarajucu poruku

lista = []
try:
    while True:
        number = input("Unesi broj ili 'Done': ")
        if number == "Done":
            break
        else:
            lista.append(int(number))

except:
    print("Pogresan unos!")

print(f"Broj unesenih elemenata je {len(lista)}.")
print(f"Srednja vrijednost je {sum(lista)/len(lista)}.")
print(f"Minimalna vrijednost je {min(lista)}.")
print(f"Maksimalna vrijednost je {max(lista)}.")
print(f"Sortirana lista je {sorted(lista)}.") 