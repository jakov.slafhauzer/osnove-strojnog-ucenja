grade = float(input('Enter a number between 0 and 1: '))

try:
    if(grade < 0 or grade > 1):
        print("Number out of interval.")
    if(grade > 0 and grade < 1):
        if grade >= 0.9:
            print("A")
        elif grade >= 0.8:
            print("B")
        elif grade >= 0.7:
            print("C")
        elif grade >= 0.6:
            print("D")
        elif grade < 0.6:
            print("F")

except:
    print("You must enter a number between 0 and 1!")