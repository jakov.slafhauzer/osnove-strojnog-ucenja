#Napišite Python skriptu koja ce ucitati tekstualnu datoteku naziva SMSSpamCollection.txt[1].
#Ova datoteka sadrži 5574 SMS poruka pri cemu su neke ozncene kao spam, a neke kao ham.
#a) Izracunajte koliki je prosjecan broj rijeci u SMS porukama koje su tipa ham, a koliko je
#prosjecan broj rijeci u porukama koje su tipa spam. 
#b) Koliko SMS poruka koje su tipa spam završava usklicnikom ?

data = open("LV1\SMSSpamCollection.txt")

ham = 0
spam = 0
spamExclamation = 0

for line in data:
    line = line.strip()
    line = line.lower()
    words = line.split()
    if words.startswith("ham"):
        ham += 1
    elif words.startswith("spam"):
        spam += 1
        if words.endswith('!'):
            spamExclamation += 1

print(f"Ham average: {ham/5574}")
print(f"Spam average: {spam/5574}")
print(f"Spam with exclamation: {spamExclamation}")


