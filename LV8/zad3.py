import numpy as np
from tensorflow import keras
from keras import layers, utils
from keras.models import load_model
from matplotlib import pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix

image = utils.load_img('test1.png', target_size = (28, 28), color_mode = "grayscale")
image = utils.img_to_array(image)
image_s = image.astype("float32") / 255
image_s = np.expand_dims(image_s, -1)
image_s = image_s.reshape(1, 784)

model = load_model('FCN/')
predicted = model.predict(image_s)

print("Predicted value is ", predicted.argmax())