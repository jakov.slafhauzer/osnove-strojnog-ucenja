import numpy as np
from tensorflow import keras
from keras import layers
from keras.models import load_model
from matplotlib import pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)
x_train_s = x_train_s.reshape(60000, 784)
x_test_s = x_test_s.reshape(10000, 784)


model = load_model('FCN/')
model.summary()

y_pred = model.predict(x_test_s)

for i in range(500):
    if y_test[i] != y_pred[i].argmax():
        plt.figure()
        plt.imshow(x_test[i], cmap='gray')
        plt.title(f"True: {y_test[i]}, Predicted: {y_pred[i].argmax()}") 
plt.show()
