import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

#Pomocu dijagrama raspršenja prikažite odnos izme ´ du gradske potrošnje goriva i emisije ¯
#C02 plinova. Komentirajte dobiveni prikaz. Kako biste bolje razumjeli odnose izmedu¯
#velicina, obojite to ˇ ckice na dijagramu raspršenja s obzirom na tip goriva

boje_goriva = {'X': 'skyblue', 'Z': 'darkblue', 'D': 'red', 'E': 'orange', 'N': 'green'}

# Prikaz dijagrama raspršenja
plt.figure(figsize=(10, 6))
for gorivo, boja in boje_goriva.items():
    gorivo = data[data['Fuel Type'] == gorivo]
    plt.scatter(gorivo['Fuel Consumption City (L/100km)'], gorivo['CO2 emissions (g/km)'], color=boja, label=gorivo, alpha=0.7)

plt.title('Odnos između gradske potrošnje goriva i emisije CO2 plinova')
plt.xlabel('Gradska potrošnja goriva (l/100km)')
plt.ylabel('CO2 emisija (g/km)')
plt.legend()
plt.grid(True)
plt.show()