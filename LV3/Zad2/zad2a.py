import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

plt.hist(data['CO2 Emissions (g/km)'], bins=20, edgecolor='black')
plt.title('Histogram emisije CO2 plinova')
plt.xlabel('CO2 emisija (g/km)')
plt.ylabel('Broj vozila')
plt.grid(True)
plt.show()


