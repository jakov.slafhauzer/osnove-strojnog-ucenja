import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

gradska_voznja = data.sort_values(by='Fuel Consumption City (L/100km)', ascending=False)
print("Najvise trose:")
print(gradska_voznja.head(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])

print("\nNajmanje trose:")
print(gradska_voznja.tail(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])



