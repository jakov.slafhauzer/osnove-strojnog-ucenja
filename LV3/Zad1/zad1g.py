import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

najpotrosniji_cetverocilindras = data[(data['Cylinders'] == 4) & (data['Fuel Type'] == 'D')].sort_values(by='Fuel Consumption City (L/100km)', ascending=False)

print(f"Vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrosnju goriva: {najpotrosniji_cetverocilindras['Model'].head(1)}")