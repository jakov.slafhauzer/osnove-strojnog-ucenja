import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

motori_izmedju = data[(data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)]

print(f"Vozila s velicinom motora izmedju 2.5 and 3.5 L: {motori_izmedju['Make'].count()}")
print(f"Prosjek C02 emisija za ove vozila: {motori_izmedju['CO2 Emissions (g/km)'].mean()} (g/km)")
