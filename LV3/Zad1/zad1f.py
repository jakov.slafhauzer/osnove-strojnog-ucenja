import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

print(f"Prosjecna gradska potrosnja u slucaju vozila koja koriste dizel: {data[data['Fuel Type'] == 'D']['Fuel Consumption City (L/100km)'].mean()}")
print(f"Prosjecna gradska potrosnja u slucaju vozila koja koriste regularni benzin: {data[data['Fuel Type'] == 'X']['Fuel Consumption City (L/100km)'].mean()}")
print(f"Medijalna vrijednost u slucaju vozila koja koriste dizel: {data[data['Fuel Type'] == 'D']['Fuel Consumption City (L/100km)'].median()}")
print(f"Medijalna vrijednost u slucaju vozila koja koriste regularni benzin: {data[data['Fuel Type'] == 'X']['Fuel Consumption City (L/100km)'].median()}")