import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

print(f"Broj Audija: {data[data['Make'] == 'Audi']['Model'].count()}")
print(f"Prosjek C02 emisija za Audija: {data[data['Make'] == 'Audi']['CO2 Emissions (g/km)'].mean()} (g/km)")