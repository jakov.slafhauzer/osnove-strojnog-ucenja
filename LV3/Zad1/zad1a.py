import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

print(f"DataFrame sadrzi {len(data)} mjerenja.\n")

print(f"Tipovi podataka: \n{data.dtypes}")

print(f"\nIzostale vrijednosti: {data.isnull().sum()}\n")

print(f"Duplicirane vrijednosti: {data.duplicated().sum()}\n")

data = data.dropna()
data = data.drop_duplicates()

kategoricke_velicine = ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type']
data[kategoricke_velicine] = data[kategoricke_velicine].astype('category')

print(f"Tipovi podataka: \n{data.dtypes}")