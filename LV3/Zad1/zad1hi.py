import pandas as pd

data = pd.read_csv('data_C02_emission.csv')

print(f"Broj vozila s rucnim tipom mjenjaca: {data[data['Transmission'].str.startswith('M')]['Model'].count()}")

print(f"Korelacija izmedju numerickih velicina: {data.corr(numeric_only=True)}")