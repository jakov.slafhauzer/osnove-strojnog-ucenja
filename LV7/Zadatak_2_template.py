import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("imgs/imgs/test_6.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()
print("Broj različitih boja u originalnoj slici:", (len(np.unique(img_array_aprox, axis=0))))

# primijeni metodu K srednjih vrijednosti
km = KMeans(n_clusters = 5, init = 'random', n_init = 5, random_state = 0)
km.fit(img_array)
cluster_centers = km.cluster_centers_
labels = km.predict(img_array)
img_array_quantized = cluster_centers[labels].reshape((w, h, d))

plt.figure()
plt.title("Rezultantna slika nakon kvantizacije boja")
plt.imshow(img_array_quantized)
plt.tight_layout()
plt.show()

print("Postavljanjem većeg broja grupa K dobit ćemo više boja i slika će biti sličnija originalu.")


#Primijenite postupak i na ostale dostupne slike

J_values = []
for i in range(1,6):
    km = KMeans(n_clusters = i, init="k-means++", n_init=5, random_state=0)
    km.fit(img_array_aprox)
    J_values.append(km.inertia_)

plt.figure()
plt.plot(range(1,6), J_values, marker=".")
plt.title("Lakat metoda")
plt.xlabel("K")
plt.ylabel("J")
plt.tight_layout()
plt.show()

unique_labels = np.unique(labels)
for i in range (len(unique_labels)):
    binary_image = labels == unique_labels[i]
    binary_image = np.reshape(binary_image, (w,h))
    plt.figure()
    plt.title(f"Binarna slika {i+1}. grupe boja")
    plt.imshow(binary_image, cmap="gray")
    plt.tight_layout()
    plt.show()
