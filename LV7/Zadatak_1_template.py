import matplotlib.pyplot as plt
import numpy as np
from scipy.cluster.hierarchy import dendrogram
from sklearn.datasets import make_blobs, make_circles, make_moons
from sklearn.cluster import KMeans, AgglomerativeClustering


def generate_data(n_samples, flagc):
    # 3 grupe
    if flagc == 1:
        random_state = 365
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
    
    # 3 grupe
    elif flagc == 2:
        random_state = 148
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

    # 4 grupe 
    elif flagc == 3:
        random_state = 148
        X, y = make_blobs(n_samples=n_samples,
                        centers = 4,
                        cluster_std=np.array([1.0, 2.5, 0.5, 3.0]),
                        random_state=random_state)
    # 2 grupe
    elif flagc == 4:
        X, y = make_circles(n_samples=n_samples, factor=.5, noise=.05)
    
    # 2 grupe  
    elif flagc == 5:
        X, y = make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

# generiranje podatkovnih primjera
X = generate_data(500, 1)

#. Primijenite metodu K srednjih vrijednosti te ponovo prikažite primjere, ali svaki primjer
#obojite ovisno o njegovoj pripadnosti pojedinoj grupi. Nekoliko puta pokrenite programski
#kod. Mijenjate broj K. Što primjecujete?

km = KMeans(n_clusters = 3, init = 'random', n_init = 5, random_state = 0)
km.fit(X)

labels = km.predict(X)

# prikazi primjere u obliku dijagrama rasprsenja
plt.figure()
plt.scatter(X[:,0],X[:,1], c=labels, cmap='viridis', s=50)
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri')
plt.show()

print("Nakon pokretanja programa s različitim vrijednostima K, možemo primijetiti kako se promjena broja klastera utječe na rezultate klasteriranja.")
print("Optimalan broj klastera je 3.")


#Mijenjajte nacin definiranja umjetnih primjera te promatrajte rezultate grupiranja podataka
#(koristite optimalni broj grupa). Kako komentirate dobivene rezultate?

for n in range(1, 6):
    X = generate_data(500, n)
    km = KMeans(n_clusters = 2, init = 'random', n_init = 5, random_state = 0)
    km.fit(X)
    labels = km.predict(X)
    plt.figure()
    plt.scatter(X[:,0],X[:,1], c=labels, cmap='viridis', s=50)
    plt.xlabel('$x_1$')
    plt.ylabel('$x_2$')
    plt.title('podatkovni primjeri')
    plt.show()

print("\nOčekujem da će K-means dobro raditi na grupiranju podataka u kružnicama i mjesecima,\nali može se suočiti s izazovom pravilnog grupiranja nepravilno raspoređenih grupa.")