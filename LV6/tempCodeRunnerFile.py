
knn_classifier = KNeighborsClassifier(n_neighbors=5)
knn_classifier.fit(X_train, y_train)

# Korak 4: Evaluacija točnosti
y_train_pred = knn_classifier.predict(X_train)
train_accuracy = accuracy_score(y_train, y_train_pred)

y_test_pred = knn_classifier.predict(X_test)
test_accuracy = accuracy_score(y_test, y_test_pred)

print("Točnost na skupu za učenje:", train_accuracy)
print("Točnost na skupu za testiranje:", test_accuracy)

plt.figure(figsize=(10, 6))
plot_decision_regions(X_test, y_test, classifier=knn_classifier)
plt.xlabel('Dob')
plt.ylabel('Procijenjena plaća')
plt.title('Granica odluke KNN modela')
plt.show()