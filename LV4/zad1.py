from sklearn import datasets
from sklearn . model_selection import train_test_split
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
import sklearn.linear_model as lm
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error, mean_absolute_percentage_error, mean_squared_error, r2_score



data = pd.read_csv('data_C02_emission.csv')

#a)
input_variables = ['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)']
output_variable = ['CO2 Emissions (g/km)']

X = data[input_variables].to_numpy()
y = data[output_variable].to_numpy()

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

#b)
plt.scatter(X_train[:, 0], y_train, color='blue', alpha=0.7)
plt.scatter(X_test[:, 0], y_test, color='red', alpha=0.7)
plt.xlabel(input_variables[0])
plt.ylabel('CO2 Emissions (g/km)')
plt.title("Ovisnost emisije CO2 plinova i Engine Size (L)")
plt.show()


#c)
scaler = StandardScaler()
X_train_n = scaler.fit_transform(X_train)
X_test_n = scaler.transform(X_test)

plt.hist(X_train[:,0], alpha=0.7, label='Prije skaliranja')
plt.hist(X_train_n[:,0], alpha=0.7, label='Nakon skaliranja')
plt.xlabel(input_variables[0])
plt.ylabel('Frekvencija')
plt.title("Histogrami ulaznih velicina prije i nakon skaliranja")
plt.legend()
plt.show()


#d)
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)
print(linearModel.coef_)


#e)
y_prediction = linearModel.predict(X_test_n)
plt.scatter(y_test, y_prediction, color='blue', alpha=0.7)
plt.figure()
plt.ylabel('Predicted')
plt.xlabel('Actual')
print("Shape of y_test:", y_test.shape)


#f)
print('MSE: {:.5f}'.format(mean_squared_error(y_test, y_prediction)))
print('RMSE: {:.5f}'.format(np.sqrt(mean_squared_error(y_test, y_prediction))))
print('MAE: {:.5f}'.format(mean_absolute_error(y_test, y_prediction)))
print('MAPE: {:.5f}'.format(mean_absolute_percentage_error(y_test, y_prediction))+'%')
print('R^2: {:.5f}'.format(r2_score(y_test, y_prediction)))